package ru.iteco.taskmanager.repository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AbstractRepository<T> {

	@NotNull
	protected final Map<String, T> map = new LinkedHashMap<>();
	
	@Nullable
	public T findByUuid(@NotNull final String uuid) {
		return map.get(uuid);
	}
	
	@Nullable
	public List<T> findAll() {
		@Nullable
		final List<T> result = new ArrayList<>();
		for (final T t : map.values()) {
			result.add(t);
		}
		return result;
	}
	
	public void clear() {
		map.clear();
	}
}
