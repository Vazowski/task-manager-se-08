package ru.iteco.taskmanager.entity;

import java.util.Date;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.Setter;

public final class Task extends AbstractEntity  {

	@NotNull
	@Getter 
	@Setter
	private String ownerId;
	@NotNull
	@Getter 
	@Setter
	private String projectUUID;
	@NotNull
	@Getter 
	@Setter
	private String name;
	@NotNull
	@Getter 
	@Setter
	private String description;
	@NotNull
	@Getter 
	@Setter
	private String dateBegin;
	@NotNull
	@Getter 
	@Setter
	private String dateEnd;
	@NotNull
	@Getter 
	@Setter
	private String type;
	
	public Task() {
    	
    }
    
    public Task(@NotNull String name, @NotNull String description, @NotNull String uuid, @NotNull String projectUuid, @NotNull String ownerId) {
    	this.ownerId = ownerId;
    	this.uuid = uuid;
    	this.projectUUID = projectUuid;
    	this.name = name;
    	this.description = description;
    	this.dateBegin = dateFormat.format(new Date());
    	this.dateEnd = dateFormat.format(new Date());
    	this.type = "Task";
    }
	
	@Override
	public String toString() {
		return "Name: " + this.name + "\nDescription: " + this.description + "\nDateBegin: " + this.dateBegin + "\nDateEnd: " + this.dateEnd + "\nUUID: " + this.uuid + "\nprojectUUID: " + this.projectUUID;
	}
}
