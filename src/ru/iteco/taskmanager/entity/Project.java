package ru.iteco.taskmanager.entity;

import java.util.Date;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.Setter;

public final class Project extends AbstractEntity {

	@NotNull
	@Getter 
	@Setter
	private String ownerId;
	@NotNull
	@Getter 
	@Setter
	private String name;
	@NotNull
	@Getter 
	@Setter
	private String description;
	@NotNull
	@Getter 
	@Setter
	private String dateBegin;
	@NotNull
	@Getter 
	@Setter
	private String dateEnd;
	@NotNull
	@Getter 
	@Setter
	private String type;
	
	public Project() {
    	
    }
    
    public Project(@NotNull final String name, @NotNull final String description, @NotNull final String uuid, @NotNull final String ownerId) {
    	this.ownerId = ownerId;
    	this.uuid = uuid;
    	this.name = name;
    	this.description = description;
    	this.dateBegin = dateFormat.format(new Date());
    	this.dateEnd = dateFormat.format(new Date());
    	this.type = "Project";
    }
    
    @NotNull
	@Override
	public String toString() {
		return "Name: " + this.name + "\nDescription: " + this.description + "\nDateBegin: " + this.dateBegin + "\nDateEnd: " + this.dateEnd + "\nUUID: " + this.uuid; 
	}
}
