package ru.iteco.taskmanager.bootstrap;

import java.util.Scanner;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;
import ru.iteco.taskmanager.service.TerminalService;
import ru.iteco.taskmanager.service.ProjectService;
import ru.iteco.taskmanager.service.TaskService;
import ru.iteco.taskmanager.service.UserService;

public final class Bootstrap implements IServiceLocator {

	private Scanner scanner;
	@NotNull
	private String inputCommand;
	@NotNull
	private static final String DEFAULT_PASSWORD = "1234";
	@NotNull
	@Getter
	private final TerminalService terminalService = new TerminalService();
	@NotNull
	@Getter
	private final ProjectService projectService = new ProjectService(new ProjectRepository(), new TaskRepository());
	@NotNull
	@Getter
	private final TaskService taskService = new TaskService(new TaskRepository());
	@NotNull
	@Getter
	private final UserService userService = new UserService();
	
	public void init(@Nullable final Class[] classes) {
		scanner = new Scanner(System.in);
		try {
			createDefaultUsers();
			registerCommand(classes);

			System.out.println("Enter command (type help for more information)");

			while (true) {
				System.out.print("> ");
				inputCommand = scanner.nextLine();
				if (!terminalService.getCommands().containsKey(inputCommand)) {
					System.out.println("Command doesn't exist");
				} else {
					terminalService.get(inputCommand).execute();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void registerCommand(@Nullable Class[] CLASSES) throws Exception {
        if (CLASSES == null) return;
        for (@Nullable Class clazz : CLASSES) {
            if (clazz == null || !AbstractCommand.class.isAssignableFrom(clazz)) continue;
            @NotNull AbstractCommand command = (AbstractCommand) clazz.newInstance();
            command.setServiceLocator(this);
            registerCommand(command);
        }
    }

    private void registerCommand(@Nullable final AbstractCommand command) {
        if (command == null) return;
        terminalService.put(command.command(), command);
    }

	
	private void createDefaultUsers() {
		userService.merge("root", userService.getPassword(DEFAULT_PASSWORD), RoleType.ADMIN);
		userService.merge("guest", userService.getPassword(DEFAULT_PASSWORD), RoleType.USER);
	}
}
