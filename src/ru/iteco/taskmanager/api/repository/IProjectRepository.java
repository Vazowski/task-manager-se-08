package ru.iteco.taskmanager.api.repository;

import java.util.List;

import ru.iteco.taskmanager.entity.Project;

public interface IProjectRepository {

	void merge(final String name, final String description, final String uuid, final String ownerId);
	Project findByName(final String name);
	Project findByName(final String name, final String ownerId);
	Project findByUuid(final String uuid, final String ownerId);
	List<Project> findAll(final String ownerId);
	void remove(final String name);
	void remove(final String uuid, final String ownerId);
}
