package ru.iteco.taskmanager.api.repository;

import ru.iteco.taskmanager.entity.Task;

public interface ITaskRepository {
	
	void merge(final String name, final String description, final String uuid, final String projectUuid, final String ownerId);
	Task findByName(final String name);
	Task findByName(final String name, final String ownerId);
	void remove(final String uuid);
	void remove(final String uuid, final String ownerId);
	void removeByName(final String name);
	void removeAllByProjectUuid(final String uuid);
}
