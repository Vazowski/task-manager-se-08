package ru.iteco.taskmanager.command.user;

import ru.iteco.taskmanager.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand{
	
	@Override
	public String command() {
		return "logout";
	}

	@Override
	public String description() {
		return "  -  user logout";
	}

	@Override
	public void execute() throws Exception {
		serviceLocator.getUserService().setCurrent("none");
		System.out.println("Done");
		serviceLocator.getTerminalService().get("login").execute();
	}

}
